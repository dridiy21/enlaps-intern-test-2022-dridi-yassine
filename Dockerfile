# The Docker container build file

# base image, feel free to change it
FROM python:3.7

COPY . .
RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt

# What is executed when calling the docker container
ENTRYPOINT [ "python", "main.py" ]