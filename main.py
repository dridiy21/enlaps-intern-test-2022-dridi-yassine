"""This is the main entrypoint of this project.
    When executed as
    python main.py input_folder/
    it should perform the neural network inference on all images in the `input_folder/` and print the results.
"""
from argparse import ArgumentParser
import sys
import os
import tensorflow as tf
import numpy as np
from keras.preprocessing import image


class_names = ['daisy', 'dandelion', 'roses', 'sunflowers', 'tulips']

def parse_args():
    """Define CLI arguments and return them.

    Feel free to modify this function if needed.

    Returns:
        Namespace: parser arguments
    """
    parser = ArgumentParser()
    parser.add_argument("input_folder", type=str, help="A folder with images to analyze.")
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    cli_args = parse_args()
    # do stuff ...
    print(f"Analyzing folder : {cli_args.input_folder}", file=sys.stderr)  # info print must be done on stderr like this for messages

    # print results on stdout
    tflite_model_path = "model.tflite"
    interpreter = tf.lite.Interpreter(model_path=tflite_model_path)
    interpreter.allocate_tensors()

    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()
    input_shape = input_details[0]['shape']
    print(f"la taille de l'entree est {input_shape}",file=sys.stderr)

    for filename in os.listdir(cli_args.input_folder):
        # initialisation des dictionnaires
        # dictionnaire intermediare
        dict = {"score": 0, "class": ""}
        # dictionnaire finale
        final_dict = {filename: {}}
        # chemin vers image
        path = os.path.join(cli_args.input_folder,filename)

        # charger l image avec les bonnes dimensions
        img = image.load_img(path, target_size=(180, 180))
        x = image.img_to_array(img)
        x = np.expand_dims(x, axis=0)
        image_tensor = np.vstack([x])
        #fin de pre_traitement de l'image

        #on peut afficher la taille de l'image input pour verifier les bonnes dimensions
        #si on fait uncomment a la ligne suivante
        #print(f"la taille de image_tensor est{image_tensor.shape}",,file=sys.stderr)

        # lancer la prediction
        interpreter.set_tensor(input_details[0]['index'], image_tensor)
        interpreter.invoke()
        classes = interpreter.get_tensor(output_details[0]['index'])
        # fin de recuperation des predictions (numpy 2D array qui contient une seule liste 1D)

        # classes est un numpy array 2D qui contient un 1D numpy array qu'on va prendre avec le max
        classes = max(classes)
        # Maintenant il faut remplacer les éléments négatifs par 0
        classes[classes < 0] = 0

        # Maintenant on va rendre les valeurs entre 0 et 1
        classes = classes / (np.sum(classes))

        #on peut afficher les scores pour verifier
        #print(f"les classes sont: {classes}",file=sys.stderr)

        # a ce stade notre 1D numpy array contenant les socres [0,1] est pret

        # Donc il faut qu'on cherche le max de cette liste et son index

        max_value = max(classes)  # Return the max value of the list.
        max_index = np.argmax(classes)  # Find the index of the max value
        # avec cette index on va accèder à la bonne catégorie dans la liste categories
        categorie = class_names[max_index]

        # update et affichage du dictionnaire final
        dict["score"] = max_value
        dict["class"] = categorie
        final_dict[filename] = dict
        print(final_dict)